type parameter = 
       | Add of int
       | Sub of int

type storage = int

let main (p,s: parameter * storage) =
 let res = match p with
            | Add n -> s + n
	    | Sub n -> s - n
 in ([] : operation list), res