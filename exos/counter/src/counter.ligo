type parameter is unit
type storage is nat

function main (const param: parameter; const s: storage)
  : (list(operation) * storage) is
  block {skip} with
  ((nil: list(operation)), s + 1n)