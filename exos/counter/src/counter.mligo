type parameter = unit
type storage = nat

let main (p,s: parameter * storage) =
  ([] : operation list), s + 1n