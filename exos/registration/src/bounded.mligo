type parameter =
       | Register of unit
       | Unregister of unit

type storage = {
  registered  : address set;
  max         : nat
}

let main (p,s: parameter * storage) =
 let res = match p with
            | Register tt ->
	      let size = Set.size s.registered
	      in if size < s.max then Set.add sender s.registered
	         else (failwith "Event is full": address set)
	    | Unregister tt -> Set.remove sender s.registered
 in ([] : operation list), { s with registered = res} 
  