"use strict";

import {Tezos} from '@taquito/taquito';
import {TezBridgeSigner} from '@taquito/tezbridge-signer';

// FIXME: Put your originated address here
let contract_address = "KT1K531zpAfyMDUUmb64oUXSkSkHQYKhm1tt";
var tk = Tezos;
// We query the sandbox node here
tk.setProvider({rpc: 'http://localhost:18731', signer: new TezBridgeSigner ()})

function render(elt) {
    document.querySelector('#info').innerHTML = elt;
}

function update_storage() {
   tk.contract.at(contract_address)
  .then(contract => contract.storage())
  .then(storage =>
	{ let reg = storage.registered.length ;
	  let remaining = storage.max - reg ; 
	  document.querySelector('#value').innerHTML = remaining;})
}

function call_contract(myFlag) {
  tk.contract.at(contract_address)
  .then(contract => {
      render("Waiting for signature...")
      if (myFlag) {return contract.methods.register('Unit').send();}
      else {return contract.methods.unregister('Unit').send();}})
  .then(op => {
    render("Sent! Waiting for confirmation...");
    return op.confirmation();})
  .then(block => {
    render("confirmed!")
    return update_storage(); })
  .catch(err => {
    console.log(err)
    render(err.message)
  })
  return null;
}

update_storage();
document.querySelector('#reg').addEventListener('click', () => call_contract(true));
document.querySelector('#unreg').addEventListener('click', () => call_contract(false));
