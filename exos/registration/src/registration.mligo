type parameter =
       | Register of unit
       | Unregister of unit

type storage = address set

let main (p,s: parameter * storage) =
 let res = match p with
            | Register tt -> Set.add sender s
	    | Unregister tt -> Set.remove sender s
 in ([] : operation list), res